//
//  DTErrors.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 19/05/23.
//

import Foundation

enum DTErrors: Error {
    case createDirectory
    case failedConverting
    case failedSaving
}

extension DTErrors: LocalizedError {

    public var title: String? {
        switch self {
        case .createDirectory:
            return NSLocalizedString("Error managing directories", comment: "Error managing directories")
        case .failedConverting:
            return NSLocalizedString("Failed converting video", comment: "Failed converting video")
        case .failedSaving:
            return NSLocalizedString("Failed saving videos", comment: "Failed saving video")
        }
    }

    public var errorDescription: String? {
        switch self {
        case .createDirectory:
            return NSLocalizedString("Could not edit directories. Check your permissions, storage space and try again.", comment: "Could not edit directories")
        case .failedConverting:
            return NSLocalizedString("Failed converting video. Check storage space, file name and try again.", comment: "Failed converting video")
        case .failedSaving:
            return NSLocalizedString(
                "Failed saving video. Check storage space, file name and try again.",
                comment: "Failed saving video"
            )
        }
    }

//    func selectError(_ error: Error) -> Self {
//        if error == NSerr
//    }
}

