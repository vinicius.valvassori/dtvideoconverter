//
//  ConversionView.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 13/05/23.
//

import SwiftUI

struct ConversionView: View {

    var inputUrl: String
    var filename: String
    var mode: ConversionService.ConversionTarget
    var quality: ConversionService.ConversionQuality
    var muted: Bool

    @State var conversionSuccess: Bool = false
    @State var conversionFailed: Bool = false
    @State var conversionFinished: Bool = false

    @StateObject var converter: ConversionService = .init()
    @EnvironmentObject
    var router: NavigationRouter

    @State var currentAlert: Alert!

    var body: some View {
        VStack {
            Text("Converting video. Please wait...")
                .padding()
            Text("Status")
            if conversionSuccess {
                Text("Conversion successful!")
                    .foregroundColor(.green)
            }
            if conversionFailed {
                Text("Conversion failed!")
                    .foregroundColor(.red)
            }
            ProgressView(value: converter.time, total: converter.totalTime)
                .scaleEffect(y: 3.0)
                .padding()
            TextEditor(text: $converter.currentLog).lineLimit(5)
                .frame(height: 200)
                .border(.orange)
                .padding()
                Button("Back to start") { router.nextStep = false }
                    .buttonStyle(DefaultButton(enabled: conversionFinished))
                    .disabled(!conversionFinished)
                if mode == .webm {
                    Button("Navigate to files") {
                        navigateToFiles()
                        router.nextStep = false
                    }
                    .buttonStyle(DefaultButton(enabled: conversionFinished))
                    .disabled(!conversionFinished)
                } else {
                    Button("Navigate to Gallery") {
                        navigateToGallery()
                        router.nextStep = false
                    }
                    .buttonStyle(DefaultButton(enabled: conversionFinished))
                    .disabled(!conversionFinished)
            }
        }
        .interactiveDismissDisabled()
        .onAppear(
            perform: {
                do {
                    try converter.convert(
                        inputUrl: inputUrl,
                        filename: filename,
                        mode: mode,
                        noSound: muted,
                        quality: quality
                    ) { success in
                        if success {
                            conversionSuccess = true
                        } else {
                            conversionFailed = true
                        }
                        conversionFinished = true
                    }
                } catch {
                    conversionFailed = true
                    conversionFinished = true
                }
            }
        )
    }

    private func navigateToFiles() {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let sharedurl = documentsUrl.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
        let furl: URL = URL(string: sharedurl)!
        if UIApplication.shared.canOpenURL(furl) {
            UIApplication.shared.open(furl, options: [:])
        }
    }

    private func navigateToGallery() {
        UIApplication.shared.open(URL(string: "photos-redirect://")!)
    }
}

struct ConversionView_Previews: PreviewProvider {
    static var previews: some View {
        ConversionView(inputUrl: "", filename: "", mode: .webm, quality: .medium, muted: true)
    }
}
