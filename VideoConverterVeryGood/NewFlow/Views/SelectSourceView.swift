//
//  SelectSourceView.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 12/05/23.
//

import SwiftUI
import Photos

struct SelectSourceView: View {

    @State var isPickingFile: Bool = false
    @State var isGalleryOpen: Bool = false
    @State var filePath: String = ""
    @State var filename: String = ""
    @State var showAlert: Bool = false
    @State var didFinishPickingImage: Bool = false

    @AppStorage("showDisclaimer") var isShowingDisclaimer: Bool = true
    @AppStorage("showPhotosAuth") var shouldShowPhotosAuth: Bool = true
    @State var photosAppNotAuthorized: Bool = false

    @EnvironmentObject
    var router: NavigationRouter

    var body: some View {
        VStack {
            Text("Select video source:")
            .padding()

            Button("From Gallery") {
                isGalleryOpen = true
            }
            .buttonStyle(DefaultButton())
            .padding()

            Button("From Files") {
                isPickingFile = true
            }
            .buttonStyle(DefaultButton())
            .padding()

            Group {
                Text("File selected")
                Text(filename)
            }
            .padding()

            if showAlert {
                Text("No file selected")
                    .foregroundColor(.red)
                    .padding()
            }
            Button("Continue") {
                if filePath.isEmpty {
                    showAlert = true
                } else {
                    showAlert = false
                    router.nextStep = true
                }
            }
            .buttonStyle(DefaultButton())
                .padding()

            if PHPhotoLibrary.authorizationStatus() == .authorized {
                Text("Saving MP4 to Photos allowed")
                    .foregroundColor(.green)
                .padding()
            } else {
                Text("Saving MP4 to Photos not allowed")
                    .foregroundColor(.red)
                .padding()
            }
        }
        .sheet(
            isPresented: $isPickingFile,
            content: {
                FilePicker(
                    types: [.init(mimeType: "video/webm")!],
                    allowMultiple: false,
                    title: ""
                ) { urls in
                    guard let videoUrl = urls.first else { return }
                    filename = videoUrl.lastPathComponent
                    filePath = videoUrl.absoluteString
                    showAlert = false
                }
            }
        )
        .sheet(
            isPresented: $isGalleryOpen,
            content: {
                ImagePickerView(
                    fileName: $filename,
                    inputUrl: $filePath
                )
            }
        )
        .sheet(
            isPresented: $router.nextStep,
            content: {
                SelectOptionsView(
                    filename: (filename as NSString).deletingPathExtension,
                    filePath: filePath
                )
                .environmentObject(router)
            }
        )
        .alert(
            "Disclaimer",
            isPresented: $isShowingDisclaimer
        ) {
            Button("OK", role: .none) {}
        } message: {
            Text("Be aware that converting videos takes a long time and consumes a good portion of battery. Take this in consideration before converting a large video or many videos in sequence.")
            .multilineTextAlignment(.leading)
        }
        .alert(
            "Photos app not authorized",
            isPresented: $photosAppNotAuthorized
        ) {
            Button("OK", role: .none) {}
        } message: {
            Text("Mp4 videos will be store in this app documents folder. Access them via Files app. If you want to save to Photos, go to Setting and allow saving.")
            .multilineTextAlignment(.leading)
        }
        .onAppear {
            if shouldShowPhotosAuth {
                PHPhotoLibrary.requestAuthorization() { status in
                    if status != .authorized {
                        photosAppNotAuthorized = true
                    }
                }
                shouldShowPhotosAuth = false
            }
        }
    }
}

struct SelectSourceView_Previews: PreviewProvider {
    static var previews: some View {
        SelectSourceView()
    }
}
