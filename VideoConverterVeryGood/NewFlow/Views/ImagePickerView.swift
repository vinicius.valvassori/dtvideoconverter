import PhotosUI
import SwiftUI

struct ImagePickerView: UIViewControllerRepresentable {
    @Binding var fileName: String
    @Binding var inputUrl: String

    @Environment(\.presentationMode) var presentationMode

    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .any(of: [.videos])
        let picker = PHPickerViewController(configuration: config)
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {}

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: PHPickerViewControllerDelegate {
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            guard !results.isEmpty else {
                self.parent.presentationMode.wrappedValue.dismiss()
                return
            }

            let imageResult = results.first!
            let provider = imageResult.itemProvider
            if provider.hasItemConformingToTypeIdentifier(UTType.movie.identifier) {
                provider.loadFileRepresentation(forTypeIdentifier: UTType.movie.identifier) { videoUrl, error in
                    guard error == nil, let url = videoUrl else { return }

                    let tempUrl = URL(fileURLWithPath: NSTemporaryDirectory() + url.lastPathComponent)
                    try? FileManager.default.copyItem(at: url, to: tempUrl)
                    self.parent.inputUrl = tempUrl.absoluteString
                    self.parent.fileName = url.lastPathComponent
                    self.parent.presentationMode.wrappedValue.dismiss()
                }
            }
        }

        private let parent: ImagePickerView
        init(_ parent: ImagePickerView) {
            self.parent = parent
        }
    }
}
