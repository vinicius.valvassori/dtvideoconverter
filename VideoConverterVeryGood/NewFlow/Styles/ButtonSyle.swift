//
//  CustomButton.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 12/05/23.
//

import SwiftUI

struct DefaultButton: ButtonStyle {
    var enabled: Bool = true
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(width: 300, height: 20)
            .padding()
            .background(enabled ? .orange : .gray)
            .foregroundColor(enabled ? .white : .black)
            .clipShape(Capsule())
            .scaleEffect(configuration.isPressed ? 0.9 : 1.0)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}
