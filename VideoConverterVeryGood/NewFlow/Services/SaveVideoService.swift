//
//  SaveVideoService.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 17/05/23.
//

import Photos
import OSLog

struct SaveVideoService {
    static func saveMp4ToPhotos(videoUrl: URL) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl) }) { saved, error in
                let logger = Logger(
                    subsystem: Bundle.main.bundleIdentifier ?? "com.digitreco.dtwebmconverter",
                    category: "save mp4"
                )
                if saved {
                    do {
                        try FileManager.default.removeItem(at: videoUrl)
                    } catch {
                        logger.error("Failed deleting temp file \(error)")
                    }
                } else {
                    logger.error("not saved \(error?.localizedDescription ?? "")")
                }
            }
    }

    static func saveVideoToFiles(
        filename: String, baseDir: URL,
        videoExtension: String,
        outputUrl: URL,
        newInputUrl: URL
    ) throws {
        let decomposedFileName = filename.decomposedStringWithCanonicalMapping
            .addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        let urlFileToRemove: URL = baseDir
            .appendingPathComponent("\(decomposedFileName).\(videoExtension)")

        if !(urlFileToRemove.absoluteString == outputUrl.absoluteString) {
            try FileManager.default.copyItem(at: urlFileToRemove, to: outputUrl) //!!!
            try FileManager.default.removeItem(at: urlFileToRemove)
        }
        try FileManager.default.removeItem(at: newInputUrl)

    }
}
