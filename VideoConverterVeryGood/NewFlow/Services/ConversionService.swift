//
//  ConversionService.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 10/05/23.
//

import Foundation
import OSLog
import Photos

class ConversionService: ObservableObject {

    @Published var currentLog: String
    @Published var time: Double = .zero
    @Published var totalTime: Double = .zero
    let logger: Logger

    init() {
        self.currentLog = ""
        logger = Logger(
            subsystem: Bundle.main.bundleIdentifier ?? "com.digitreco.dtwebmconverter",
            category: "save service"
        )
    }

    enum ConversionTarget: String, CaseIterable, Identifiable {
        case webm
        case mp4
        public var id: Self { self }
    }

    enum ConversionQuality: String, CaseIterable, Identifiable {
        case low
        case medium
        case high
        public var id: Self { self }
    }

    func convert(
        inputUrl: String,
        filename: String,
        mode: ConversionTarget,
        noSound: Bool,
        quality: ConversionQuality,
        onCompleted completionHandler: @escaping (_ status: Bool) -> Void
    ) throws {
        self.log("Conversion starting...")
        let videoExtension: String = mode == .webm ? "webm" : "mp4"

        let cacheDir = getCacheDirectory()
        guard let originalUrl = URL(string: inputUrl) else {
            completionHandler(false)
            return
        }

        let newInputUrl = cacheDir.appendingPathComponent("temp_video.\(videoExtension)")
        try setupTempVideoPaths(originalUrl: originalUrl, tempFileUrl: newInputUrl)


        var baseDir = getDocumentsDirectory()
        baseDir = baseDir.appendingPathComponent(videoExtension, isDirectory: true)

        try FileManager.default.createDirectory(at: baseDir, withIntermediateDirectories: true)

        let outputUrl = baseDir.appendingPathComponent("\(filename).\(videoExtension)")

        let crfQuality: Int = setupQuality(quality)

        let muted: String = noSound ? "-an" : ""

        let toWebmArguments: String = "-y -i \(newInputUrl) -c:v libvpx-vp9 -crf \(crfQuality) \(muted) \(outputUrl)"
        let toMp4Arguments: String = "-y -i \(newInputUrl) -c:v mpeg4 -crf \(crfQuality) \(muted) \(outputUrl)"

        let arguments = mode == .webm ? toWebmArguments : toMp4Arguments

        let mediaInformationSession = FFprobeKit.getMediaInformation(inputUrl)
        let mediaInformation =  mediaInformationSession?.getMediaInformation()

        self.log("Conversion started... plz wait...")

        let session = FFmpegKit.executeAsync(arguments) { [weak self] ffmpegSession in
            guard let session = ffmpegSession else {
                return
            }
            let state: SessionState = session.getState()
            let returnCode: ReturnCode = session.getReturnCode()
            if ReturnCode.isSuccess(returnCode) {
                self?.logger.log("conversao acabou com sucesso")
                self?.log("Conversion finished successfully")

                do {
                    try SaveVideoService.saveVideoToFiles(
                        filename: filename,
                        baseDir: baseDir,
                        videoExtension: videoExtension,
                        outputUrl: outputUrl,
                        newInputUrl: newInputUrl
                    )
                    if PHPhotoLibrary.authorizationStatus() == .authorized && mode == .mp4 {
                        SaveVideoService.saveMp4ToPhotos(videoUrl: outputUrl)
                    }
                } catch {
                    self?.logger.error("error copying and deleting whole name file \(error)")
                }

                completionHandler(ReturnCode.isSuccess(returnCode))

            } else {
                self?.logger.error("conversao acabou com falha")
                self?.logger.error("state: \(FFmpegKitConfig.sessionState(toString: state) ?? "") | rc: \(returnCode)")
                self?.log("Conversion failed. Go back and try again.")
                completionHandler(false)
            }
        } withLogCallback: { [weak self] log in
            if let logs = log {
                let message: String = logs.getMessage()
                self?.logger.info("\(message)")
            }
        } withStatisticsCallback: { [weak self] stats in
            if let statistics = stats {
                DispatchQueue.main.async {
                    self?.time = Double(statistics.getTime()) / 1000.0
                    self?.totalTime = Double(mediaInformation?.getDuration() ?? "30000.0") ?? 30000.0
                    self?.logger.info("time =\(self?.time ?? 0.0)")
                    self?.logger.info("total=\(self?.totalTime ?? 0.0)")
                }
            }
        }
    }

    private func log(_ string: String) {
        DispatchQueue.main.async {
            self.currentLog.append(string + "\n")
        }
    }

    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    private func getCacheDirectory() -> URL {
        let paths = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    private func setupQuality(_ quality: ConversionQuality) -> Int {
        switch quality {
        case .low:
            return 50
        case .high:
            return 5
        default:
            return 32
        }
    }

    private func setupTempVideoPaths(originalUrl: URL, tempFileUrl: URL) throws {
        do {
            try FileManager.default.copyItem(at: originalUrl, to: tempFileUrl)
        } catch {
            try FileManager.default.removeItem(at: tempFileUrl)
            try FileManager.default.copyItem(at: originalUrl, to: tempFileUrl)
        }
    }
}
