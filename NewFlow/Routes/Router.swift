//
//  Router.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 13/05/23.
//

import Foundation

class NavigationRouter: ObservableObject {

    @Published
    var nextStep: Bool = false
}
