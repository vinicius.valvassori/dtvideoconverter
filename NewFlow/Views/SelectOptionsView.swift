//
//  SelectOptionsView.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 13/05/23.
//

import SwiftUI

struct SelectOptionsView: View {

    @State var fileTypeSelection: ConversionService.ConversionTarget = .webm
    @State var qualityTypeSelection: ConversionService.ConversionQuality = .medium
    @State var muted: Bool = true

    @State var filename: String
    @State var filePath: String

    @State var isShowingConversionScreen: Bool = false

    @FocusState var filenameIsFocused
    @Environment(\.presentationMode) var presentationMode

    @EnvironmentObject
    var router: NavigationRouter

    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("(Optional) Edit file name")) {
                    TextField(
                        "Selected file",
                        text: $filename,
                        prompt: Text("Selected file")
                    )
                    .focused($filenameIsFocused)
                    .padding()
                }
                Section(header: Text("Convert for")) {
                    Picker(
                        "",
                        selection: $fileTypeSelection
                    ) {
                        ForEach(ConversionService.ConversionTarget.allCases) { option in
                            Text(option.rawValue)
                        }
                    }
                    .pickerStyle(.segmented)
                }
                Section(header: Text("Quality")) {
                    Picker(
                        "",
                        selection: $qualityTypeSelection
                    ) {
                        ForEach(ConversionService.ConversionQuality.allCases) { option in
                            Text(option.rawValue)
                        }
                    }
                    .pickerStyle(.segmented)
                }
                Section(header: Text("Mute")) {
                    Toggle("Remove audio?", isOn: $muted)
                }
                Button("START CONVERTING") {
                    isShowingConversionScreen = true
                }
                .buttonStyle(GrowingButton())
            }
            .sheet(isPresented: $isShowingConversionScreen, content: {
                ConversionView(
                    inputUrl: filePath,
                    filename: filename,
                    mode: fileTypeSelection,
                    quality: qualityTypeSelection
                )
                .environmentObject(router)
            })
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading, content: {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "xmark")
                    })
                })
            }
        }
    }
}

struct SelectOptionsView_Previews: PreviewProvider {
    static var previews: some View {
        SelectOptionsView(filename: "", filePath: "")
    }
}
