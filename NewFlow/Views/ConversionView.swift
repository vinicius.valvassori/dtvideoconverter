//
//  ConversionView.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 13/05/23.
//

import SwiftUI

struct ConversionView: View {

    var inputUrl: String
    var filename: String
    var mode: ConversionService.ConversionTarget
    var quality: ConversionService.ConversionQuality

    @State var isShowingAlert: Bool = false
    @StateObject var converter: ConversionService = .init()
    @EnvironmentObject
    var router: NavigationRouter
    
    var body: some View {
        VStack {
            Text("Converting video. Please wait...")
            Group {
                Text("Status")
                TextEditor(text: $converter.currentLog).lineLimit(5)
                    .frame(height: 300).disabled(true)
                    .border(.orange)
            }.padding()
        }
        .interactiveDismissDisabled()
        .onAppear(perform: {
            converter.convert(
                inputUrl: inputUrl,
                filename: filename,
                mode: mode,
                quality: quality
                ) { success in
                    if success {
                        isShowingAlert = true
                    }
                }
        })
        .alert(
            "Video converted successfully",
            isPresented: $isShowingAlert, actions: {
            Button(
                "OK",
                role: .cancel
            ) {
                router.nextStep = false
            }
        })
    }
}

struct ConversionView_Previews: PreviewProvider {
    static var previews: some View {
        ConversionView(inputUrl: "", filename: "", mode: .webm, quality: .medium)
    }
}
