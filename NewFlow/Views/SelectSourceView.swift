//
//  SelectSourceView.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 12/05/23.
//

import SwiftUI

struct SelectSourceView: View {

    @State var isPickingFile: Bool = false
    @State var isGalleryOpen: Bool = false
    @State var filePath: String = ""
    @State var filename: String = ""
    @State var showAlert: Bool = false
    @State var didFinishPickingImage: Bool = false

    @EnvironmentObject
    var router: NavigationRouter

    var body: some View {
        VStack {
            Text("Select video source:")
            .padding()

            Button("From Gallery") {
                isGalleryOpen = true
            }
            .buttonStyle(GrowingButton())
            .padding()

            Button("From Files") {
                isPickingFile = true
            }
            .buttonStyle(GrowingButton())
            .padding()

            Group {
                Text("File selected")
                Text(filename)
            }
            .padding()

            if showAlert {
                Text("No file selected")
                    .foregroundColor(.red)
                    .padding()
            }
            Button("Continue") {
                if filePath.isEmpty {
                    showAlert = true
                } else {
                    showAlert = false
                    router.nextStep = true
                }
            }
            .buttonStyle(GrowingButton())
                .padding()
        }
        .sheet(
            isPresented: $isPickingFile,
            content: {
                FilePicker(
                    types: [.init(mimeType: "video/webm")!],
                    allowMultiple: false,
                    title: ""
                ) { urls in
                    guard let videoUrl = urls.first else { return }
                    filename = videoUrl.lastPathComponent
                    filePath = videoUrl.absoluteString
                    showAlert = false
                }
            }
        )
        .sheet(
            isPresented: $isGalleryOpen,
            content: {
                ImagePickerView(
                    fileName: $filename,
                    inputUrl: $filePath
                )
            }
        )
        .sheet(
            isPresented: $router.nextStep,
            content: {
                SelectOptionsView(filename: filename, filePath: filePath)
                    .environmentObject(router)
            }
        )
    }
}

struct SelectSourceView_Previews: PreviewProvider {
    static var previews: some View {
        SelectSourceView()
    }
}
