//
//  ConversionService.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 10/05/23.
//

import Foundation

class ConversionService: ObservableObject {

    @Published var currentLog: String

    init() {
        self.currentLog = ""
    }

    enum ConversionTarget: String, CaseIterable, Identifiable {
        case webm
        case mp4
        public var id: Self { self }
    }

    enum ConversionQuality: String, CaseIterable, Identifiable {
        case low
        case medium
        case high
        public var id: Self { self }
    }

    func convert(
        inputUrl: String,
        filename: String,
        mode: ConversionTarget,
        noSound: Bool = true,
        quality: ConversionQuality,
        onCompleted completionHandler: @escaping (_ status: Bool) -> Void
    ) {
        var outputUrl = getDocumentsDirectory()
        outputUrl = outputUrl.appendingPathComponent(mode == .webm ? "webm" : "mp4", isDirectory: true)

        do {
            try FileManager.default.createDirectory(at: outputUrl, withIntermediateDirectories: true)
        } catch {
            print("error")
        }

        outputUrl = outputUrl.appendingPathComponent("\(filename).\(mode == .webm ? "webm" : "mp4")")

        var crfQuality = 32
        switch quality {
        case .low:
            crfQuality = 50
        case .high:
            crfQuality = 5
        default:
            crfQuality = 32
        }

        let toWebmArguments: String = "-y -i \(inputUrl) -c:v libvpx-vp9 -crf \(crfQuality) \(noSound ? "-an" : "") \(outputUrl)"
        let toMp4Arguments: String = "-y -i \(inputUrl) -c:v mpeg4 -crf \(crfQuality) \(outputUrl)"

        let arguments = mode == .webm ? toWebmArguments : toMp4Arguments

        let session = FFmpegKit.executeAsync(arguments) { ffmpegSession in
            guard let session = ffmpegSession else {
                return
            }
            let state: SessionState = session.getState()
            let returnCode: ReturnCode = session.getReturnCode()
            if ReturnCode.isSuccess(returnCode) {
                print("conversao acabou com sucesso")
                self.log("Conversion finished successfully")
                completionHandler(ReturnCode.isSuccess(returnCode))
            } else {
                print("conversao acabou com falha")
                print("state: \(FFmpegKitConfig.sessionState(toString: state) ?? "") | rc: \(returnCode)")
                self.log("Conversion failed.")
                completionHandler(false)
            }
        } withLogCallback: { [weak self] log in
            if let logs = log {
                self?.log(logs.getMessage() ?? "")
            }
        } withStatisticsCallback: { stats in
            if let statistics = stats {
                print(statistics.getVideoFrameNumber())
                print(statistics.getSize())
            }
        }
    }

    private func log(_ string: String) {
        DispatchQueue.main.async {
            var temp = string
            temp.append("\n")
            temp.append(self.currentLog)
            temp.append("\n")
            self.currentLog = temp
        }
    }

    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
