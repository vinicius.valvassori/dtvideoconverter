//
//  ContentView.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 09/05/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
//            TabView {
//                ConvertWebmView()
//                    .tabItem{
//                        Label("To Webm", image: "")
//                    }
//                ConvertFromWebmView()
//                    .tabItem {
//                        Label("Webm To MP4", image: "")
//                    }
//            }
            SelectSourceView()
                .environmentObject(NavigationRouter())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
