//
//  VideoConverterVeryGoodApp.swift
//  VideoConverterVeryGood
//
//  Created by Vinicius Valvassori on 09/05/23.
//

import SwiftUI

@main
struct VideoConverterVeryGoodApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
